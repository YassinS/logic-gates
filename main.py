import sys
def NAND(a,b):
    a = int(a)
    b=int(b)
    try:
        if a == 1 and b == 1:
            return 0
        elif a>1 and b>1 or a<0 and b<0:
            print("1/0 only")
            return -1
        else:
            return 1
    except:
        return "(nand error)1/0 only"


def NOT(a):
    a=int(a)
    try:
        if a>1 or a<0:
            print("1/0 only")
            return -1
        if a == 1:
            return 0
        if a == 0:
            return 1
    except:
        return "(not error)1/0 only"


def AND(a,b):
    a=int(a)
    b=int(b)
    try:
        if a == 1 and b == 1:
            return 1
        elif a>1 and b>1 or a<0 and b<0:
            print("1/0 only")
            return -1
        else:
            return 0
    except:
        return "(and error)1/0 only"

def OR(a,b):
    a=int(a)
    b=int(b)
    try:
        if a == 0 and b == 0:
            return 0
        elif a>1 and b>1 or a<0 and b<0:
           print("1/0 only")
           return -1
        else:
            return 1
    except:
        return "1/0 only"

def XOR(a,b):
    a=int(a)
    b=int(b)
    try:
        if a != b:
            return 1
        elif a>1 and b>1 or a<0 and b<0:
            print("1/0 only")
            return -1
        else:
            return 0
    except:
        return "1/0 only"

def truthtable(gate,n=False):
    if gate == NOT:
        print("     ")
        print(gate)
        print("a | out")
        print("1 | ",gate(1))  
        print("0 | ",gate(0))
    if gate != NOT and n == True:
        print(gate)
        print("              ")
        print("a | b | out")
        print("1 | 1 | ",NOT(gate(1,1))) 
        print("1 | 0 | ",NOT(gate(1,0)))
        print("0 | 1 | ",NOT(gate(0,1)))   
        print("0 | 0 | ",NOT(gate(0,0)))

    else:
        print(gate)
        print("              ")
        print("a | b | out")
        print("1 | 1 | ",gate(1,1))  
        print("1 | 0 | ",gate(1,0))
        print("0 | 1 | ",gate(0,1))    
        print("0 | 0 | ",gate(0,0))


while True:
    print("by Yassin Sassi")
    cmd = input("user@skynet $ ")      #TODO
    if cmd == "help":   
        print("List of commands: ")
        print("OR(a,b)")
        print("AND(a,b)")
        print("NOT(a)")
        print("NAND(a,b)")
        print("truthtable")
        print("(q)uit") 
    elif cmd == "OR":
         a=input("a= ")
         b=input("b= ")
         print(OR(a,b))
    elif cmd == "q":
        break
    elif cmd == "AND":
        a=input("a= ")
        b=input("b= ")
        print(AND(a,b))
    elif cmd == "NOT":
        a=input("a= ")
        print(NOT(a))
    elif cmd == "NAND":
        a=input("a= ")
        b=input("b= ")
        print(NAND(a,b))
    elif cmd == "XOR":
        a=input("a= ")
        b=input("b= ")
        print(XOR(a,b))
    elif cmd == "truthtable":
        truthtable(AND)
        truthtable(OR)
        truthtable(XOR)
        truthtable(NAND)
            
    else:
        print("Command: ",cmd, "not recognized. Enter help for a"+
        "list of commands")